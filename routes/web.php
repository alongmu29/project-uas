<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/anime-wathcing', function () {
    return view('anime-watching');
})->name('anime-watching');

Route::get('/anime-detail', function () {
    return view('anime-details');
})->name('anime-details');

Route::get('/berita', function () {
    return view('blog-details');
})->name('blog-details');

Route::get('/blog', function () {
    return view('blog');
})->name('blog');

Route::get('/kategori', function () {
    return view('categories');
})->name('categories');

Route::get('/Overlord-2', function () {
    return view('eps2');
})->name('eps2');

Route::get('/Overlord-3', function () {
    return view('eps3');
})->name('eps3');

Route::get('register', [UserController::class, 'register'])->name('register');
Route::post('register.action', [UserController::class, 'register_action'])->name('register.action');
Route::get('login', [UserController::class, 'login'])->name('login');
Route::post('login.action', [UserController::class, 'login_action'])->name('login.action');
Route::get('password', [UserController::class, 'password'])->name('password');
Route::post('password.action', [UserController::class, 'password_action'])->name('password.action');
Route::get('logout', [UserController::class, 'logout'])->name('logout');

