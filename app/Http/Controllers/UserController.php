<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\UserController;

class UserController extends Controller
{
    public function register()
    {
    $data['title'] = 'Register';
    return view('user/register', $data);
}

public function register_action(Request $request)
{
    $request->validate([
    'name' => 'required',
    'username' => 'required|unique:tb_user',
    'password' => 'required',
    'password_confirmation' => 'required|same:password',
   ]);
   $user = new User([
    'name' => $request->name,
    'username' => $request->username,
    'password' => Hash::make($request->username),
   ]);
   $user->save();
   return redirect()->route('login')->with('success', 'Registrasi sukses, Silakan Login');
}

public function login()
{
$data['title'] = 'login';
return view('user/login', $data);
}

public function login_action(Request $request)
{
    $request->validate([
    'username' => 'required',
    'password' => 'required',
   ]);
   if(Auth::attempt(['username' => $request->username, 'password' => $request->password]))
        {
            $request->session()->regenerate();
            return redirect()->intended('/');
        }

        $user->save();
        return back()->withErrors('password', 'Username atau Password yand anda masukkan salah!');
}

public function password()
    {
        $data['title'] = 'Change Password';
        return view('user/password', $data);
    }

    public function password_action(Request $request)
    {
        $request->validate([
            'old_password' => 'required|current_password',
            'new_password' => 'required|confirmed',
            
        ]);
        
        $user = User::find(Auth::id());
        $user->password = Hash::make($request->new_password);
        $user->save();
        $request->session()->regenerate();
        return back()->with('success', 'Password telah diubah!');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }

}
