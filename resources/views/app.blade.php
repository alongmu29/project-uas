<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', $title)</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <style>
        .my-hero {
    height: 699px;
    left: 474px;
    position: absolute;
    top: -15px;
    width: 885px;
    z-index: -9998;
    

}

.lingkaran-hero {
    background: linear-gradient(180deg, 
     rgb(205, 1, 1) 10.7%, rgb(255, 22, 22) 51.7%);
    border-radius: 354px/331.5px;
    height: 663px;
    left: 650px;
    position: absolute;
    top: 30px;
    width: 708px;
    z-index: -9999;
}

    body {
        background-color: #101010;
    }

    .card-body{
        border: 1px solid rgb(0, 0, 0);
        background-color: #222222;
        color: antiquewhite;
        box-shadow: 5px 5px 8px #1f1f1f;
    }

    .logo-anime {
    height: 141px;
    left: 73px;
    
    position: absolute;
    top: 0;
    width: 150px;
}

    </style>
      <img src="{{ asset('front')}}/img/logo.png" class="rounded float-end" alt="...">
    <div class="container">
        <h1 style="color: aliceblue;">@yield('title', $title)</h1>
        @yield('content')
    </div>
    <div class="lingkaran-hero"></div>
    <img src="{{ asset('front')}}/img/hero2.png" alt="alt" class="my-hero" />
    
</body>
</html>