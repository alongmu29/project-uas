@extends('app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            @if (session('success'))
            <p class="alert alert-success">{{session('success')}}</p>  
            @endif
            @if ($errors->any())
            @foreach($errors->all() as $err)
            <p class="alert alert-danger">{{ $err }}</p>                
            @endforeach
            @endif
            <div class="card">
                <div class="card-body">
            <form method="POST" action="{{ route('password.action') }}">
                @csrf
               
                
                <div class="mb-3">
                    <label>Old Password <span class="text-danger">*</span></label>
                    <input  type="password" class="form-control" name="old_password" />
                </div>
                <div class="mb-3">
                    <label>New Password <span class="text-danger">*</span></label>
                    <input  type="password" class="form-control" name="new_password" />
                </div>
                <div class="mb-3">
                    <label>New Password Confirmation <span class="text-danger">*</span></label>
                    <input  type="password" class="form-control" name="new_password_confirmation" />
                </div>
                <br />
                <br />
                <br />

                <div class="mb-3">
                    <center>
                    <button class="btn btn-primary">Change</button>
    
                    <a class="btn btn-danger" href="{{ route('home') }}">Back</a>
                    </center>
                </div>
            </form>
        </div>
    </div>
        </div>
    </div>
    @endsection
