@extends('app')
@section('content')
<br />
<br />
<br />
<br />
<br />
    <div class="row">
        <div class="col-md-6">
            @if (session('success'))
                <p class="alert alert-success">{{ (session('success')) }}</p>
            @endif
            @if ($errors->any())
            @foreach($errors->all() as $err)
            <p class="alert alert-danger">{{ $err }}</p>                
            @endforeach
            @endif
            <form class="card-body" method="POST" action="{{ route('login.action') }}">
                @csrf
                

                <div class="mb-3">
                    <label for="">Username <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="username" value="{{ old('username') }}" />
                </div>

                <div class="mb-3">
                    <label for="">Password <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password" />
                </div>

           
                <div class="mb-3">
                    <button class="btn btn-primary">Login</button>
                    <a href="{{ route('home') }}" class="btn btn-danger">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection