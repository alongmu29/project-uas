<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AnimeKu | Streaming Anime Gratis</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('front') }}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('front') }}/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('front') }}/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('front') }}/css/plyr.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('front') }}/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('front') }}/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('front') }}/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('front') }}/css/style.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @auth
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="./index.html">
                            <img src="{{ asset('front') }}/img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="{{ route('home') }}">Beranda</a></li>
                                <li><a href="{{ route('categories') }}">Kategori<span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="{{ route('categories') }}">Kategori</a></li>
                                        <li><a href="{{ route('anime-details') }}">Detail Anime</a></li>
                                        <li><a href="{{ route('anime-watching') }}">Anime Watching</a></li>
                                        <li><a href="{{ route('blog-details') }}">Blog Detail</a></li>
                                        
                                    </ul>
                                </li>
                                <li><a href="{{ route('blog') }}">Blog</a></li>
                                <li><a href="#">Kontak</a></li>
                      
                                    <li><a href="{{ route('password') }}" class="">Password</a></li>
                                    <li><a style="color:rgb(199, 0, 0);" href="{{ route('logout') }}" class="">Log Out</a></li>
                                </ul>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        
                            <ul>
                                <li><a href="#">{{ Auth::user()->name }}</span></a>
                                    
                                </li>
            
                            </ul>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
        
    @endauth


        <!-- Header Section Begin -->
        @guest
        <header class="header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="header__logo">
                            <a href="./index.html">
                                <img src="{{ asset('front') }}/img/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="header__nav">
                            <nav class="header__menu mobile-menu">
                                <ul>
                                    <li class="active"><a href="{{ route('home') }}">Beranda</a></li>
                                    <li><a href="{{ route('categories') }}">Kategori<span class="arrow_carrot-down"></span></a>
                                        <ul class="dropdown">
                                            <li><a href="{{ route('categories') }}">Kategori</a></li>
                                            <li><a href="{{ route('anime-details') }}">Detail Anime</a></li>
                                            <li><a href="{{ route('anime-watching') }}">Anime Watching</a></li>
                                            <li><a href="{{ route('blog-details') }}">Blog Detail</a></li>
                                            
                                        </ul>
                                    </li>
                                    <li><a href="{{ route('blog') }}">Blog</a></li>
                                    <li><a href="#">Kontak</a></li>
                                      <li><a href="{{ route('login') }}" class="cola1">Login</a></li>
                                        <li><a href="{{ route('register') }}" class="cola2">Register</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    
                    </div>
                </div>
                <div id="mobile-menu-wrap"></div>
            </div>
        </header>
            
        @endguest
    <!-- Header End -->

    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <a href="./categories.html">Categories</a>
                        <a href="#">Magic</a>
                        <span>Overload | Season 1</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Anime Section Begin -->
    <section class="anime-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="anime__video__player">
                        <video id="player" playsinline controls data-poster="{{ asset('front') }}//img/hero/hero-11.jpg">
                            <source src="{{ asset('front') }}/videos/1.mp4" type="video/mp4" />
                            <!-- Captions are optional -->
                            <track kind="captions" label="English captions" src="#" srclang="en" default />
                        </video>
                    </div>
                    <div class="anime__details__episodes">
                        <div class="section-title">
                            <h5>List Name</h5>
                        </div>
                        <a href="{{ route('anime-watching') }}">Ep 01</a>
                        <a href="{{ route('eps2') }}">Ep 02</a>
                        <a href="{{ route('eps3') }}">Ep 03</a>
                        <a href="{{ route('anime-watching') }}">Ep 04</a>
                        <a href="{{ route('anime-watching') }}">Ep 05</a>
                        <a href="{{ route('anime-watching') }}">Ep 06</a>
                        <a href="{{ route('anime-watching') }}">Ep 07</a>
                        <a href="{{ route('anime-watching') }}">Ep 08</a>
                        <a href="{{ route('anime-watching') }}">Ep 09</a>
                        <a href="{{ route('anime-watching') }}">Ep 10</a>
                        <a href="{{ route('anime-watching') }}">Ep 11</a>
                        <a href="{{ route('anime-watching') }}">Ep 12</a>
                        <a href="{{ route('anime-watching') }}">Ep 13</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="anime__details__review">
                        <div class="section-title">
                            <h5>Reviews</h5>
                        </div>
                        <div class="anime__review__item">
                            <div class="anime__review__item__pic">
                                <img src="{{ asset('front') }}/img/anime/review-1.jpg" alt="">
                            </div>
                            
                    </div>
                    <div class="anime__details__form">
                        <div class="section-title">
                            <h5>Your Comment</h5>
                        </div>
                        <form action="#">
                            <textarea placeholder="Your Comment"></textarea>
                            <button type="submit"><i class="fa fa-location-arrow"></i> Review</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Anime Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="page-up">
            <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer__logo">
                        <a href="./index.html"><img src="{{ asset('front') }}/img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer__nav">
                        <ul>
                            <li class="active"><a href="./index.html">Homepage</a></li>
                            <li><a href="./categories.html">Categories</a></li>
                            <li><a href="./blog.html">Our Blog</a></li>
                            <li><a href="#">Contacts</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3">
                    

                  </div>
              </div>
          </div>
      </footer>
      <!-- Footer Section End -->

      <!-- Search model Begin -->
      <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="icon_close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="{{ asset('front') }}/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('front') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('front') }}/js/player.js"></script>
    <script src="{{ asset('front') }}/js/jquery.nice-select.min.js"></script>
    <script src="{{ asset('front') }}/js/mixitup.min.js"></script>
    <script src="{{ asset('front') }}/js/jquery.slicknav.js"></script>
    <script src="{{ asset('front') }}/js/owl.carousel.min.js"></script>
    <script src="{{ asset('front') }}/js/main.js"></script>

</body>

</html>